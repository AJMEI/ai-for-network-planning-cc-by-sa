import csv

from matplotlib import rcParams
import matplotlib.pyplot as plt
import numpy as np

from Self_Net_TSP.dataset import DataGenerator
from Self_Net_TSP.tsp_with_ortools import Solver
from utils.sampler import Sampler


def visualize_2d_trip(trips, title="Trips", show=True):
    if show:
        rcParams.update({'font.size': 16})

    max_x = 0
    min_x = 10000
    max_y = 0
    min_y = 10000
    for trip in trips:
        if len(trip):
            # Plot cities
            plt.scatter(trip[:, 0], trip[:, 1], s=200)

            # Plot tour
            tour = np.array(list(range(len(trip))) + [0])
            X = trip[tour, 0]
            Y = trip[tour, 1]
            plt.plot(X, Y, "--", markersize=100)

            # Annotate cities with order
            labels = range(len(trip))
            for i, (x, y) in zip(labels, (zip(X, Y))):
                plt.annotate("%.1f" % trip[i, 2], xy=(x, y))

            max_x = np.max([max_x, np.max(X)])
            max_y = np.max([max_y, np.max(Y)])
            min_x = np.min([min_x, np.min(X)])
            min_y = np.min([min_y, np.min(Y)])

    plt.title(title)
    plt.xlim(min_x - abs(min_x * 0.1), max_x + abs(0.1 * max_x))
    plt.ylim(min_y - abs(min_y * 0.1), max_y + abs(0.1 * max_y))

    if show:
        plt.show()


def visualize_2d_trip_2(trips1, trips2, title1="Title1", title2="Title2"):
    plt.figure(figsize=(30, 30))
    rcParams.update({'font.size': 22})

    plt.subplot(121)
    visualize_2d_trip(trips1, title1, False)

    plt.subplot(122)
    visualize_2d_trip(trips2, title2, False)

    plt.show()


def visualize_2d_trip_n(*trips_and_titles, **params):

    plots_count = len(trips_and_titles)

    window_size = 10
    plt.figure(figsize=(window_size * plots_count, window_size))
    rcParams.update({'font.size': 16})

    subplot = 100 + plots_count * 10
    for i, (trips, title) in enumerate(trips_and_titles):
        plt.subplot(subplot + i + 1)
        visualize_2d_trip(trips, title, False)

    if 'show' not in params or params['show']:
        plt.show()


def compute_optimal_trip(p):
    if len(p) > 1:
        solver = Solver(len(p))
        dataset_generator = DataGenerator(solver)

        return dataset_generator.solve_instance_2(p[:, :-1])

    return [], [], 0


def create_batch_from_csv(game, batch_size, input_file, include_separator=True, normalize_demands=True):
    with open(input_file, 'r') as file:
        reader = csv.reader(file, delimiter=',')
        batch_input = []
        for row in reader:
            new_row = [x for x in map(float, row)]
            batch_input.append(new_row)

        batch_input = np.array(batch_input)
        # Send to 0
        batch_input_ = batch_input.copy()
        batch_input_[:, 0] = batch_input[:,0] - np.min(batch_input[:,0])
        batch_input_[:, 1] = batch_input[:,1] - np.min(batch_input[:,1])
        # End send to 0
        guessed_grid_width = np.max(batch_input_[:, :2].flatten())
        print('guessed grid width: %d' % guessed_grid_width)
        decorrelated_batch, ind = Sampler.decorrelate_instance(batch_input_, grid_width=guessed_grid_width) # TODO has to be as trained
        batch_input = batch_input[ind, :]
        if normalize_demands:
            demands = decorrelated_batch[:, 2]
            demands = demands / np.sum(demands.flatten())
            decorrelated_batch[:, 2] = demands

        # This is used for training in the combinatorial network
        sequence_length = batch_input.shape[0]

        if include_separator:
            batch_input = np.concatenate([batch_input, game.SEPARATOR], axis=0)
            decorrelated_batch = np.concatenate([decorrelated_batch, game.SEPARATOR], axis=0)

        # Repeat batch_size times
        decorrelated_batch = np.expand_dims(decorrelated_batch, axis=0)
        batch_input = np.expand_dims(batch_input, axis=0)

        decorrelated_batch = np.tile(decorrelated_batch, [batch_size, 1, 1])
        batch_input = np.tile(batch_input, [batch_size, 1, 1])

    return sequence_length, batch_input, decorrelated_batch


def bin_array(num, m):
    """Convert a positive integer num into an m-bit bit vector"""
    return np.array(list(np.binary_repr(num).zfill(m))).astype(np.int8)


def normalize_costs(costs_):

    if costs_ > 0:
        # C1 = 151441.0
        # C2 = 3600.0
        C3 = 600000.0
        C4 = 295000.0
        C5 = C4

        costs_ -= C3
        costs_ /= C5

    return costs_
