from functools import partial

import tensorflow as tf
import numpy as np

# Tensor summaries for TensorBoard visualization
from combinatorial_network.critic import Critic
from combinatorial_network.decoder import PointerDecoder
from combinatorial_network.encoder import AttentiveEncoder


def variable_summaries(name, var, with_max_min=False):
    with tf.name_scope(name):
        mean = tf.reduce_mean(var)
        tf.summary.scalar('mean', mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('stddev', stddev)

        if with_max_min:
            tf.summary.scalar('max', tf.reduce_max(var))
            tf.summary.scalar('min', tf.reduce_min(var))


class Actor(object):

    def __init__(self, batch_size,
                 max_length,
                 input_dimension,
                 hidden_dim,
                 num_heads,
                 num_stacks,
                 inference_mode,
                 temperature,
                 logit_clip,
                 init_baseline,
                 alpha,
                 lr1_start,
                 lr1_decay_rate,
                 lr1_decay_step):
        # Data config
        self.batch_size = batch_size  # batch size
        self.max_length = max_length  # input sequence length (number of cities)
        self.input_dimension = input_dimension  # dimension of a city (coordinates)

        # Reward config
        self.avg_baseline = tf.Variable(init_baseline, trainable=False,
                                        name="moving_avg_baseline")  # moving baseline for Reinforce
        self.alpha = alpha  # moving average update

        # Training config (actor)
        self.global_step = tf.Variable(0, trainable=False, name="global_step")  # global step
        self.lr1_start = lr1_start  # initial learning rate
        self.lr1_decay_rate = lr1_decay_rate  # learning rate decay rate
        self.lr1_decay_step = lr1_decay_step  # learning rate decay step

        # Power mean for reward
        power_mean_start = 5.0
        power_mean_end = 1.0
        power_mean_end_after = 30000  # reach power_mean_end after this many steps
        power_mean_step = 1000

        # Compute decay rate such that after 'power_mean_end_after' the value is 'power_mean_end'
        decay_rate = (np.log(power_mean_start) - np.log(power_mean_end))/power_mean_end_after

        power_mean_decay_rate = tf.constant(decay_rate, dtype=tf.float32)

        # noinspection PyUnusedLocal,PyTypeChecker
        def stepwise_exponential_decay():
            k = tf.reduce_min([tf.math.ceil(self.global_step / power_mean_step),
                               tf.math.floor(self.global_step / power_mean_step)])

            step = k*power_mean_step
            e = power_mean_start*tf.exp(-tf.cast(step, dtype=tf.float32) * power_mean_decay_rate)

            return tf.cond(e > power_mean_end, lambda: e, lambda: power_mean_end)

        # noinspection PyUnusedLocal
        def static_decay():
            return power_mean_start

        # a * e^(-t*r) = b
        # ln(a) - t*r = ln(b)
        # r = (ln(a) - ln(b))/t

        # noinspection PyTypeChecker
        # self.power_mean_degree = tf.cond(self.global_step <= power_mean_threshold_step,
        #                                 static_decay,
        #                                 lambda: power_mean_end)

        self.power_mean_degree = stepwise_exponential_decay()

        # Training config (encoder)
        self.hidden_dim = hidden_dim
        self.num_heads = num_heads
        self.num_stacks = num_stacks
        self.inference_mode = inference_mode
        self.is_training_ = tf.placeholder(dtype=tf.bool, shape=[], name='is_training')

        # Training config (decoder)
        self.temperature = temperature
        self.logit_clip = logit_clip

        # Training config (critic)
        self.global_step2 = tf.Variable(0, trainable=False, name="global_step2")  # global step
        self.lr2_start = lr1_start  # initial learning rate
        self.lr2_decay_rate = lr1_decay_rate  # learning rate decay rate
        self.lr2_decay_step = lr1_decay_step  # learning rate decay step

        # Other network modules
        self.critic = None
        self.decoder = None

        # Network outputs / inputs
        self.positions = None
        self.log_softmax = None
        self.base_op = None
        self.lr1 = None
        self.opt1 = None
        self.reward_baseline = None
        self.loss1 = None
        self.train_step1 = None
        self.lr2 = None
        self.opt2 = None
        self.loss2 = None
        self.train_step2 = None
        self.ordered_input_ = None
        self.distances = None
        self.costs = None
        self.reward = None  # tf.placeholder(tf.float32, [self.batch_size], name='rewards')
        self.partition_sizes = None

        # Tensor block holding the input sequences [Batch Size, Sequence Length, Features]
        self.input_ = tf.placeholder(tf.float32, [self.batch_size, self.max_length, self.input_dimension],
                                     name="input_coordinates")

        self.original_input_ = tf.placeholder(tf.float32, [self.batch_size, self.max_length, self.input_dimension],
                                              name="input_coordinates")

        self.costs_heuristic_ = tf.placeholder(tf.float32, [None], name='costs_heuristic')
        self.costs_heuristic = None

        self.build_partitioning()
        self.build_reward()
        self.build_critic()
        self.build_optimization()
        self.merged = tf.summary.merge_all()

    @staticmethod
    def reduce_power_mean(input_tensor, p=1.0, axis=None):
        length = tf.cast(tf.shape(input_tensor)[axis] if axis is not None else tf.size(input_tensor), dtype=tf.float32)
        m = tf.pow(tf.reduce_sum(tf.pow(input_tensor, p), axis=axis) / length, 1.0 / p)

        return m

    @staticmethod
    def compute_travel_distance(p, max_cost=0.0):
        max_cost = float(max_cost)

        def dist():
            P = tf.concat([p, tf.expand_dims(p[0, :], axis=0)], axis=0)
            delta_x = tf.square(P[1:, 0] - P[:-1, 0])
            delta_y = tf.square(P[1:, 1] - P[:-1, 1])
            distance_ = tf.sqrt(delta_x + delta_y)
            return tf.reduce_sum(distance_)

        number_of_points = tf.shape(p)[0]
        distance = tf.cond(number_of_points > 1, dist, lambda: max_cost)

        return distance

    @staticmethod
    def compute_network_cost(p, max_cost=0.0):
        # Note here demand needs to be >0 so that it has consumers! otherwise we do not have any producers

        C1 = 151441.0
        C2 = 3600.0
        # C3 = 600000.0
        C4 = 295000.0
        C5 = C4

        def cost():
            P = tf.concat([p, tf.expand_dims(p[0, :], axis=0)], axis=0)
            # P = p
            delta_x = tf.square(P[1:, 0] - P[:-1, 0])
            delta_y = tf.square(P[1:, 1] - P[:-1, 1])
            travel_distance = tf.reduce_sum(tf.sqrt(delta_x + delta_y))

            demand_sum = tf.reduce_sum(p[:, 2])
            total_cost_ = ((C1 + C2 * demand_sum) * travel_distance + C4 * demand_sum) / C5

            return total_cost_

        number_of_points = tf.shape(p)[0]
        total_cost = tf.cond(number_of_points >= 1, cost, lambda: tf.constant(float(max_cost)))

        return total_cost

    @staticmethod
    def compute_distances(max_length_, permutation_, points_, split_position_):
        pos = tf.where(split_position_)[0][0]

        partition1, _, partition2 = tf.split(permutation_,
                                             num_or_size_splits=
                                             [pos,
                                              1,
                                              max_length_ - pos - 1],
                                             name="split",
                                             axis=0)

        ordered_input1 = tf.gather(points_, partition1)
        ordered_input2 = tf.gather(points_, partition2)

        distance1 = Actor.compute_travel_distance(ordered_input1, 0.0)
        distance2 = Actor.compute_travel_distance(ordered_input2, 0.0)
        return distance1, distance2

    @staticmethod
    def compute_costs(max_length_, permutation_, points_, split_position_):
        pos = tf.where(split_position_)[0][0]

        partition1, _, partition2 = tf.split(permutation_,
                                             num_or_size_splits=
                                             [pos,
                                              1,
                                              max_length_ - pos - 1],
                                             name="split",
                                             axis=0)

        ordered_input1 = tf.gather(points_, partition1)
        ordered_input2 = tf.gather(points_, partition2)

        cost1 = Actor.compute_network_cost(ordered_input1, 0.0)
        cost2 = Actor.compute_network_cost(ordered_input2, 0.0)

        min_partition_size = tf.reduce_min([tf.shape(ordered_input1)[0], tf.shape(ordered_input2)[0]])

        return cost1, cost2, min_partition_size

    def build_reward(self):
        with tf.name_scope('environment'):
            permutations = self.positions[:, :-1]

            b = tf.equal(permutations, self.max_length - 1)
            nested = permutations, self.original_input_, b

            f = partial(Actor.compute_costs, self.max_length)

            costs1, costs2, self.partition_sizes = tf.map_fn(lambda x: f(*x), nested, dtype=(tf.float32, tf.float32,
                                                                                             tf.int32))

            costs_both = tf.stack([costs1, costs2], axis=1)

            self.costs = tf.reduce_sum(costs_both, axis=1)

            # reduction = partial(Actor.power_mean, p=self.power_mean_degree, axis=1)
            self.reward = Actor.reduce_power_mean(costs_both, p=1.0, axis=1)
            self.costs_heuristic = Actor.reduce_power_mean(self.costs_heuristic_, p=1.0)

            variable_summaries('costs', self.costs, with_max_min=True)
            variable_summaries('reward', self.reward, with_max_min=True)

        with tf.name_scope('heuristic'):
            tf.summary.scalar('costs_heuristic', self.costs_heuristic)
            variable_summaries('diff_heuristic_ours', self.costs_heuristic - self.reward, with_max_min=True)

        with tf.name_scope('costs_per_partition'):
            variable_summaries('costs1', costs1)
            variable_summaries('costs2', costs2)

        with tf.name_scope('costs_reduction'):
            tf.summary.scalar('power_mean_degree', self.power_mean_degree)

        with tf.name_scope('partitioning'):
            self.partition_sizes = tf.cast(tf.stack(self.partition_sizes), tf.float32)
            variable_summaries('min_partition_sizes', self.partition_sizes, with_max_min=True)

    def build_partitioning(self):
        with tf.variable_scope("encoder"):
            encoder = AttentiveEncoder(self.batch_size,
                                       self.max_length,
                                       self.input_dimension,
                                       self.hidden_dim,
                                       self.num_heads,
                                       self.num_stacks,
                                       self.is_training_)

            encoder_output = encoder.encode(self.input_)

        with tf.variable_scope('decoder'):
            # Ptr-net returns permutations (self.positions), with their log-probability for backprop
            self.decoder = PointerDecoder(encoder_output, self.is_training_, self.temperature, self.logit_clip)
            self.positions, self.log_softmax = self.decoder.loop_decode()
            variable_summaries('log_softmax', self.log_softmax, with_max_min=True)

    def build_critic(self):
        with tf.variable_scope("critic"):
            # Critic predicts reward (parametric baseline for REINFORCE)
            critic_encoder = AttentiveEncoder(self.batch_size,
                                              self.max_length,
                                              self.input_dimension,
                                              self.hidden_dim,
                                              self.num_heads,
                                              self.num_stacks,
                                              not self.inference_mode)

            self.critic = Critic(critic_encoder)
            self.critic.predict_rewards(self.input_)
            variable_summaries('predictions', self.critic.predictions, with_max_min=True)

    def build_optimization(self):
        # Update moving_mean and moving_variance for batch normalization layers
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            with tf.name_scope('baseline'):
                # Update baseline
                reward_mean, reward_var = tf.nn.moments(self.reward, axes=[0])
                self.base_op = tf.assign(self.avg_baseline,
                                         self.alpha * self.avg_baseline + (1.0 - self.alpha) * reward_mean)
                tf.summary.scalar('average baseline', self.avg_baseline)

            with tf.name_scope('reinforce'):
                # Actor learning rate
                self.lr1 = tf.train.exponential_decay(self.lr1_start, self.global_step, self.lr1_decay_step,
                                                      self.lr1_decay_rate, staircase=False, name="learning_rate1")
                # Optimizer
                self.opt1 = tf.train.AdamOptimizer(learning_rate=self.lr1, beta1=0.9, beta2=0.99, epsilon=0.0000001)
                # Discounted reward
                self.reward_baseline = tf.stop_gradient(
                    self.reward - self.avg_baseline - self.critic.predictions)  # [Batch size, 1]
                variable_summaries('reward_baseline', self.reward_baseline, with_max_min=True)
                # Loss
                self.loss1 = tf.reduce_mean(self.reward_baseline * self.log_softmax, 0)
                tf.summary.scalar('loss1', self.loss1)
                # Minimize step
                gvs = self.opt1.compute_gradients(self.loss1)
                capped_gvs = [(tf.clip_by_norm(grad, 1.), var) for grad, var in gvs if grad is not None]  # L2 clip
                self.train_step1 = self.opt1.apply_gradients(capped_gvs, global_step=self.global_step)

            with tf.name_scope('state_value'):
                # Critic learning rate
                self.lr2 = tf.train.exponential_decay(self.lr2_start, self.global_step2, self.lr2_decay_step,
                                                      self.lr2_decay_rate, staircase=False, name="learning_rate1")
                # Optimizer
                self.opt2 = tf.train.AdamOptimizer(learning_rate=self.lr2, beta1=0.9, beta2=0.99, epsilon=0.0000001)
                # Loss
                weights_ = 1.0  # weights_ = tf.exp(self.log_softmax-tf.reduce_max(self.log_softmax)) # probs / max_prob
                self.loss2 = tf.losses.mean_squared_error(self.reward - self.avg_baseline,
                                                          self.critic.predictions,
                                                          weights=weights_)
                tf.summary.scalar('loss2', self.loss2)
                # Minimize step
                gvs2 = self.opt2.compute_gradients(self.loss2)
                capped_gvs2 = [(tf.clip_by_norm(grad, 1.), var) for grad, var in gvs2 if grad is not None]  # L2 clip
                self.train_step2 = self.opt1.apply_gradients(capped_gvs2, global_step=self.global_step2)
