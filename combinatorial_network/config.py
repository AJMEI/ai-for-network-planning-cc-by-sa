# -*- coding: utf-8 -*-
import argparse

parser = argparse.ArgumentParser(description='Configuration file')
arg_lists = []


def add_argument_group(name):
    arg = parser.add_argument_group(name)
    arg_lists.append(arg)
    return arg


def str2bool(v):
    return v.lower() in ('true', '1')


# Network
net_arg = add_argument_group('Network')
net_arg.add_argument('--hidden_dim', type=int, default=192, help='actor LSTM num_neurons')
net_arg.add_argument('--num_heads', type=int, default=16, help='actor input embedding')
net_arg.add_argument('--num_stacks', type=int, default=3, help='actor LSTM num_neurons')

# Data
data_arg = add_argument_group('Data')
data_arg.add_argument('--batch_size', type=int, default=3000, help='batch size')
data_arg.add_argument('--input_dimension', type=int, default=3, help='city dimension')
data_arg.add_argument('--grid_width', type=int, default=20, help='grid width')
data_arg.add_argument('--max_length', type=int, default=25, help='number of deliveries')

# Training / test parameters
train_arg = add_argument_group('Training')
train_arg.add_argument('--nb_epoch', type=int, default=100000, help='nb epoch')
train_arg.add_argument('--lr1_start', type=float, default=0.001, help='actor learning rate')
train_arg.add_argument('--lr1_decay_step', type=int, default=5000, help='lr1 decay step')
train_arg.add_argument('--lr1_decay_rate', type=float, default=0.96, help='lr1 decay rate')

train_arg.add_argument('--alpha', type=float, default=0.99, help='update factor moving average baseline')
train_arg.add_argument('--init_baseline', type=float, default=5.0, help='initial baseline - REINFORCE')

train_arg.add_argument('--temperature', type=float, default=3.0, help='pointer_net initial temperature')
train_arg.add_argument('--C', type=float, default=10.0, help='pointer_net tan clipping')

# Misc
misc_arg = add_argument_group('User options')
misc_arg.add_argument('--inference_mode', type=str2bool, default=True,
                      help='switch to inference mode when model is trained')
misc_arg.add_argument('--restore_model', type=str2bool, default=True, help='whether or not model is retrieved')
misc_arg.add_argument('--validation_batches', type=int, default=10, help='number of batches to use for validation')
misc_arg.add_argument('--restore_from', type=str, default='save/models/plots_test/actor.ckpt-99000',
                      help='loader sub directory')
misc_arg.add_argument('--save_to', type=str, default='save/models/plots_test', help='saver sub directory')
misc_arg.add_argument('--log_dir', type=str, default='summary/combinatorial/train', help='summary writer log directory')
misc_arg.add_argument('--save_every_n_episodes', type=int, default=1000, help='saves a checkpoint every [n] steps')

misc_arg.add_argument('--input_file', type=str, default='',
                      help='use input file for problem instance (csv file with 3 columns)')
misc_arg.add_argument('--save_solutions_to', type=str, default='',
                      help='save solution as csv to this path')

# Those were missing
misc_arg.add_argument('--save_plots_to', type=str, default='evaluation/first_level_plots', help='Where to store plots from the inference')
misc_arg.add_argument('--show_plots', type=str2bool, default=False, help='Should the plots be shown during inference')

misc_arg.add_argument('--save_solutions', type=str2bool, default=True, help='If the solutions are to be saved')
misc_arg.add_argument('--save_plots', type=str2bool, default=True, help='If the plots are to be saved')
misc_arg.add_argument('--save_name', type=str, default="0", help='If the plots are to be saved')


def get_config():
    config, unparsed = parser.parse_known_args()
    return config, unparsed


def print_config():
    config, _ = get_config()
    print('\n')
    print('Data Config:')
    print('* Batch size:', config.batch_size)
    print('* Sequence length:', config.max_length)
    print('* City coordinates:', config.input_dimension)
    print('\n')
    print('Network Config:')
    print('* Restored model:', config.restore_model)
    print('* Actor hidden_dim (embed / num neurons):', config.hidden_dim)
    print('* Actor tan clipping:', config.C)
    print('\n')

    if config.inference_mode:
        print('Training Config:')
        print('* Nb epoch:', config.nb_epoch)
        print('* Temperature:', config.temperature)
        print('* Actor learning rate (init,decay_step,decay_rate):', config.lr1_start, config.lr1_decay_step,
              config.lr1_decay_rate)
    else:
        print('Testing Config:')
    print('* Summary writer log dir:', config.log_dir)
    print('\n')
