import pandas as pd
import os
import numpy as np
import datetime
import matplotlib.pyplot as plt

print(datetime.date.today())

# print type(groningen_grid7.csv)

gridpoints = pd.read_csv('groningen_grid8.csv')


def coordinateFinder(x, y):
    try:
        coords = gridpoints[(gridpoints.x == x) & (gridpoints.y == y)]
        return coords.lat.values[0], coords.lon.values[0]
    except:
        return 0, 0


def coordinateFinderTodor(x, y):
    lon = 6.55586125e+00 +  3.79009502e-04*x -3.80605350e-07*y
    lat = 5.32124297e+01 +  1.08240702e-07*x  + 3.17917062e-04*y
    return lat, lon


def writekml(filename, connections):
    # open the kml-file (note it's a simple text-file with .kml at the end)
    f = open(filename, 'w')
    # write some definitions to make google earth understand our messy lines
    f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
    f.write(
        '<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom"><Document>\n')
    # loop over de connections to be drawn
    for i in range(len(connections)):
        f.write('<Placemark>\n')
        print(connections[i])
        lat1 = (connections[i])[0]
        lon1 = (connections[i])[1]
        lat2 = (connections[i])[2]
        lon2 = (connections[i])[3]
        thickness = (connections[i])[4]
        f.write('<Style><LineStyle><width>' + str(thickness) + '</width></LineStyle></Style>\n')
        f.write('<LineString><tessellate>1</tessellate><coordinates>\n')
        f.write(str(lon1) + ',' + str(lat1) + ',0\n')
        f.write(str(lon2) + ',' + str(lat2) + ',0\n')
        # close the xml body of the placemark
        f.write('</coordinates></LineString></Placemark>\n')
    # write the closing statements of the KML-document body and close the file
    f.write('</Document></kml>')
    f.close()


folder = './'
x = os.listdir(folder)

for filen in x:
    if filen.endswith('_all.csv'):

        connectionfile = pd.read_csv(folder + filen)
        dotfile = pd.read_csv("./groningen_grid8.csv")
        print(connectionfile.head())
        connections = []

        for index in connectionfile.index.values:
            x1 = connectionfile.x1[index]
            x2 = connectionfile.x2[index]

            lat1, lon1 = coordinateFinderTodor(x1, connectionfile.y1[index])
            lat2, lon2 = coordinateFinderTodor(x2, connectionfile.y2[index])
            thickness = connectionfile.capacity[index]
            if lat1 != 0 or lat2 != 0 or lon1 != 0 or lon2 != 0:
                connections.append([lat1, lon1, lat2, lon2, np.log(connectionfile.capacity[index])])

        print(connections)
        # Debug
        fig, ax = plt.subplots()
        for i in range(len(connectionfile)):
            ax.plot([connectionfile.x1.values[i], connectionfile.x2.values[i]],
                    [connectionfile.y1.values[i], connectionfile.y2.values[i]])
            ax.scatter(dotfile.x, dotfile.y, s=5)
        plt.show()
        writekml('kmlfiles/' + filen[:-4] + '_' + str(datetime.date.today()) + '.kml', connections)
        print('kmlfiles/' + filen[:-4] + '_' + str(datetime.date.today()) + '.kml', 'written')
