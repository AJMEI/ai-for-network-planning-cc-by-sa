"""
Use this to generate the smaller grids that are the input for the neural network
Note the file has the form:
X,Y, Demand in grid coordinates, and no header
"""
import argparse

import numpy as np
import pandas as pd
from sklearn.cluster import AgglomerativeClustering as ahc

if __name__ == '__main__':
    # Do the argparse
    parser = argparse.ArgumentParser(description='Data Partition Script')
    parser.add_argument('--input_file', help="input_file", type=str, default='input_data/goeningen_raw.csv')
    args = parser.parse_args()

    data = pd.read_csv(args.input_file, header=None)
    data.columns = ['X', 'Y', 'D']
    # 19 classes is what it takes
    c_numbers = 19  # number of clusters
    classes = ahc(n_clusters=c_numbers).fit_predict(data[['X', 'Y']])
    classes = np.array(classes)

    for i in range(c_numbers):
        ids = np.where(classes == i)[0]
        cluster = data.values[ids, :]
        np.savetxt("input_data/{}.csv".format(i), cluster, delimiter=",")
