import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import minimize


def process_file(f, id, save_plots_dir, level = 1):
    """
    :param f: file location
    :param id: id in the lists
    :return: producer locations and producer capacities. Dataframe of connections
    """
    f = np.genfromtxt(f, delimiter=',')
    f = pd.DataFrame(f)
    #f = pd.read_csv(f, header=None)
    f.columns = ['x', 'y', 'd', 'c']
    # Do plot
    # C is class
    f.d = f.d #/ np.min(f.d.values)

    f_0 = f[f['c'] < 0.5]
    f_1 = f[f['c'] > 0.5]

    con1, demand1 = process_connections(f_0)  # connections and demand of the first grid
    con2, demand2 = process_connections(f_1)  # connections and demand of the second grid

    p1, p2, dist = find_find_centers(con1, con2)
    new_producer = (p1*demand1 + p2*demand2) / (demand1 + demand2)  # find the mean

    fig, ax = plt.subplots()
    ax.scatter(p1[0], p1[1], s=demand1 * 10, c="red", marker='*')
    ax.scatter(p2[0], p2[1], s=demand2 * 10, c="red", marker='*')
    ax.scatter(new_producer[0], new_producer[1], s=(demand1 + demand2)*11, c="red", marker='*')
    ax.scatter(f.x.values, f.y.values, s=f.d.values * 20, c=f.c.values)
    ax.plot([p1[0], p2[0]], [p1[1], p2[1]], linestyle='dashed', color='yellow')
    for i in range(len(con1)):
        ax.plot([con1[i, 0], con1[i, 2]], [con1[i, 1], con1[i, 3]], linestyle='dashed', color='green')
    for i in range(len(con2)):
        ax.plot([con2[i, 0], con2[i, 2]], [con2[i, 1], con2[i, 3]], linestyle='dashed', color='cyan')
    con1 = np.hstack([con1, demand1 * np.ones([con1.shape[0], 1])])
    con2 = np.hstack([con2, demand2 * np.ones([con2.shape[0], 1])])

    # Save figure
    plt.savefig(save_plots_dir + "{}.png".format(id))

    producers = []
    # producers have position and capacity of the both combined so that if one fails the other supplies

    producers.append([p1[0], p1[1], demand1])
    producers.append([p2[0], p2[1], demand2])


    if level == 1:
        producers = np.array([new_producer[0], new_producer[1], demand2+demand1])
        con_prod1 = np.array([p1[0], p1[1], new_producer[0], new_producer[1], dist / 2, np.max([demand1, demand2])])
        con_prod2 = np.array([new_producer[0], new_producer[1], p2[0], p2[1], dist / 2, np.max([demand1, demand2])])
        all_connections = np.vstack([con1, con2, con_prod1, con_prod2])
    else:
        producers = []
        producers.append([p1[0], p1[1], demand1+demand2])
        producers.append([p2[0], p2[1], demand2+demand1])
        producers = np.array(producers)
        con_prod = np.array([p1[0], p1[1], p2[0], p2[1], dist, np.max([demand1, demand2])])
        all_connections = np.vstack([con1, con2, con_prod])
        # Fix the connection

    # Save intermediate results
    return all_connections, producers


def helper_distance_calc(theta, s1, s2):
    """
    :param theta0:
    :param theta1:
    :param s1:
    :param s2:
    :return: the distance between the two line segments at theta0, theta1
    """
    p1 = s1[:2] * theta[0] + (1 - theta[0]) * s1[2: 4]
    p2 = s2[:2] * theta[1] + (1 - theta[1]) * s2[2:4]
    return np.linalg.norm(p1 - p2)


def optimize_dist(segment_1, segment_2):
    """
    :param segment_1: x1,y1, x2,y2
    :param segment_2: x1,y1, x2,y2 arrays
    :return: theta_0, theta1, min_distance
    """
    # Define constraints
    cons = [{'type': 'ineq', 'fun': lambda x: x[0]},
            {'type': 'ineq', 'fun': lambda x: x[1]},
            {'type': 'ineq', 'fun': lambda x: 1 - x[0]},
            {'type': 'ineq', 'fun': lambda x: 1 - x[1]}]

    success = False
    while not success:
        min_res = minimize(helper_distance_calc, np.array([0.5, 0.5]), constraints=cons, args=(segment_1, segment_2), tol=1e-3, options={'maxiter':1e10})
        success = min_res.success
    theta = min_res.x
    dist = min_res.fun
    return theta, dist


def find_find_centers(c1, c2):
    """
    Finds the two centers. Returns only positions
    :param c1: subgrid 1
    :param c2: subgrid2
    :return: [[x_c1,y_c1], [x_c2,y_c2]]
    """
    c1_segment = None
    c2_segment = None
    theta = 0
    dist = 99999999999999999999999999999999999999999999999999
    for i in range(len(c1)):
        for j in range(len(c2)):
            theta_, dist_ = optimize_dist(c1[i, :4], c2[j, :4])
            if dist_ <= dist:
                theta = theta_
                dist = dist_
                c1_segment = i
                c2_segment = j
    # Found minimum
    # Calculate centers, and return the line and centers
    p1 = c1[c1_segment, :2] * theta[0] + c1[c1_segment, 2:4] * (1 - theta[0])
    p2 = c2[c2_segment, :2] * theta[1] + c2[c2_segment, 2:4] * (1 - theta[1])
    return p1, p2, dist


def process_connections(df):
    """
    We calculate the connections and demands here
    :param df: Dataframe describing only the subgrid.
    :return:
    """
    demand = df.d.sum()
    con_list = []
    d = df[['x', 'y']].values  # easier to work with the np array than with the pandas df. ALlows for negative indexing
    for i in range(-1, len(df) - 1):
        # -1 to the leghth of df so that we close the circle
        clen = np.sqrt((d[i, 0] - d[i + 1, 0]) ** 2 + (d[i, 1] - d[i + 1, 1]) ** 2)
        c = [d[i, 0], d[i, 1], d[i + 1, 0], d[i + 1, 1], clen]  # x1,y1, x2,y2
        con_list.append(c)
    return np.array(con_list), demand
