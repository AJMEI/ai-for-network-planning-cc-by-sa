# Planning networks with reinforcement learning
RL system for planning of multi-commodity network. 

# Licence
CC  Creative commons licence
BY  Attribution to authors 
SA  Shared under same principles

## Requirements and installation
Our code was tested with _python 3.6.x_ and _tensorflow 1.12.x_. The easiest way to run our
models is to install the requirements with [pipenv](https://realpython.com/pipenv-guide/).
  ```
  $ git clone https://github.com/tkostov/RL_Planning_S.git
  $ cd RL_Planning_S
  $ pipenv install
  ```
# Problem Definition

Given a set of points $$P \subset Grid(12,12) : p_i \in P$$ and a set of demands $$d_i$$, find a disjunct partitioning partitioning $$P_1,P_2$$ so that $$P1 \cap P_2= \phi$$ and  $$P1 \cup P_2= P$$ so that the $$TPS(P_1)$$ and $$TSP(P_2)$$ result in a solution to the network planning 
problem with minimum cumulative cost. The objective function is defined as:

$$cost(P_1, P_2, TSP(P_1), TSP(P_2)) = producercost(SUM(d_i) + SUM(d_y))*\alpha_{producer} + const_{producer}*2 +$$
$$(|P_1|+1)*const_{connection factor} + SUM(d_i)*\alpha_{connection} + (|P_2|+1)*const_{connection factor} +$$
$$SUM(d_y)*\alpha_{connection}$$ 

for $$d_i$$ being the corresponding demands for points in $$P_1$$ and $$d_y$$ being the corresponding demands for points in $$P_2$$. The main aim is to minimise the objective function, which will be denoted as $$cost(.)$$.

The problem with minimizing $$cost(.)$$ is that the problem that is it related to is compared of two NP-Hard problems - travelling salesman problem (TSP) and a metric facility location problem (MFLP) (http://www.or.uni-bonn.de/~vygen/files/fl.pdf) TODO add citation for TSP if neceserry
This implies that even for small instances to find optimal solution the number of possible combinations is too large to be handled efficiently:
Possible partitions $$|partitions| = 2^{n-1}$$. If we exclude the only trivial partition this number decreases by 1.
Possible ways to connect the graph (TSP problem) can be expressed thru permutations. For a set of $$n$$ elements there are $$(n-1)!$$.    $$-1$$ since we fix the first and the last element to be the same.

This points to the total complexity being in $$O(2^{(n-1)} * ((n-1)!))$$.(Note, there exists a tighter bound but to do the math for this would take quite some space.) Hence for 25 if we run the exhaustive search we need to perform $$O(4.3372487 * 10^{29})$$ steps.

In this work we leverage the Bellman optimality principle(Citation needed) applied to reinforcement learning.
 
## Models

To tackle the aforementioned problem we tried two different approaches: 

1) a **partitioning network** that learns the best way to assign the consumers to
   a set of 2 producers.

2) a **combinatorial network** that learns both partitioning and optimal circles at the same time.

  
## Partitioning Network

### Model Description

The proposed model uses two neural reinforcement networks to produce an approximately optimal solution to the given 
problem. The first network is used to produce a partition (left side of the image), while the second network is used to calculate an approximate
optimal circle (approximation of the TSP problem) within the predicted partitions (see orange box on the right part of the image). 

In the following sections we describe the architecture of the first model. The second model based on [Neural Combinatorial Optimization with Reinforcement Learning](https://arxiv.org/abs/1611.09940),
by Bello et al.,

![Diagram 1](https://github.com/tkostov/RL_Planning_S/raw/master/img/diagram.jpeg)

As in state of the art networks that are used to approximate NP-Hard combinatorial problems, we modeled our network
using a encoder-decoder structure, which is described in the following sections.

#### Input

The input of the network is a sequence of 3 dimensional points, the consumers. The first two dimensions denote the 
position on the grid, while the third dimension indicates the demand of the consumer. Training data is sampled at random
from a Gaussian Mixture Model. To leverage training we apply PCA to the x- and y-coordinates of the consumers. The demands
are not changed.    

#### Embedding Layer

In order to learn a representation of the sequence that is optimized for the task at hand, we project the input points to a d-dimensional
embedding. This is done by convoluting the input sequence with a stack 1x3 one-dimensional convolution filters. 

#### Encoder 

The embedded representation of the sequence is then encoded using a multi-head attention mechanism as described in 
[Attention is all you need](https://arxiv.org/abs/1706.03762), by Ashish Vaswani et al.
This helps the network understand which parts of the sequence to pay the most attention to, in order to produce good
partitions. The result of this attentive encoding is a vector that gives a contextualized representation
of the input sequence, i.e. it implicitly encodes the grid points and their relationships to one another. This will give
the network enough information to produce sensible partitioning results.

#### Decoder

The output produced by the encoder is now used to predict a partitioning over the input sequence. This is done by using
a decoding mechanism, that, conditioned on the output of the encoder, produces a vector representation for each consumer 
in the input sequence. These vectors will then be used to predict the partition membership of each consumer. 

The decoder is modeled as a bidirectional RNN with LSTM units. The LSTMs cell state vectors are initialized randomly 
and learned during training. The hidden state vectors are initialized with the output of the encoder. The input of the RNN
is the same as the input of the encoder. We configured the network, such that the outputs of each time step are concatenated
and then used to predict the partitioning of the sequence.  

#### Prediction & Reward 

For each consumer in the sequence we then predict the probability of it being in the first partition and the second
partition respectively. We then repeatedly sample from this distribution and train our network based on the costs the
sample partition produces. We compare these costs to the costs produced by a simple heuristic, by simply subtracting the costs
of each solution from each other. Our network is positively reinforced when it produces less costly networks than the ones
proposed by the heuristic.

#### TSP-Approximation

The calculation of the network costs implies computing an optimal - i.e. the shortest -circular path within each partition. We use
a pre-trained neural model as proposed by Bello et al. to achieve this. Note that is equivalent to solving the Travel Salesman Problem, which is known to be NP-hard.
Therefore, the solutions produced by the TSP-Network are not always optimal. However, as experiments show in [Neural Combinatorial Optimization with Reinforcement Learning](https://arxiv.org/abs/1611.09940) the network
is capable of achieving very competitive solutions.   

#### Loss & Optimization   

Based on the reward signal computed in the previous, we then adapt the weights at each step using policy gradient descend. 
The approach here is to positively reinforce the network, when it produces low cost networks with high confidence. 
This is modelled mathematically by computing the cross-entropy between the partition distribution produced by the network
and the sampled solution and multiplying it by the cost of the network:  

    L(p, s, r) = crossentropy(p, s) . r

Here `p` denotes the distribution produced by the network, `s` the sampled partition and `r` the rewards yielded by the 
chosen solution. 
This function defines the loss of our network. We minimize `L` by adjusting the network's weights using a RMSPropOptimizer. 

### Results
TODO?

### Usage
```
usage: run_partitioning_network.py [-h] [--lstm_state_size LSTM_STATE_SIZE]
               [--use_attention USE_ATTENTION] [--batch_size BATCH_SIZE]
               [--grid_width GRID_WIDTH] [--min_consumers MIN_CONSUMERS]
               [--max_consumers MAX_CONSUMERS] [--lr LR]
               [--max_episodes MAX_EPISODES]
               [--game_repetitions GAME_REPETITIONS]
               [--save_every_n_episodes SAVE_EVERY_N_EPISODES]
               [--examples EXAMPLES] [--show_plots SHOW_PLOTS]
               [--save_plots SAVE_PLOTS] [--save_plots_to SAVE_PLOTS_TO]
               [--save_solutions SAVE_SOLUTIONS]
               [--save_solutions_to SAVE_SOLUTIONS_TO]
               [--inference_mode INFERENCE_MODE]
               [--restore_model RESTORE_MODEL] [--save_to SAVE_TO]
               [--restore_from RESTORE_FROM] [--log_dir LOG_DIR]
Configuration file
optional arguments:
  -h, --help            show this help message and exit
Network:
  --lstm_state_size LSTM_STATE_SIZE
                        LSTM state size
  --use_attention USE_ATTENTION
                        whether or not to use attention mechanism
Data:
  --batch_size BATCH_SIZE
                        batch size
  --grid_width GRID_WIDTH
                        grid with
  --min_consumers MIN_CONSUMERS
                        min amount of consumers]
  --max_consumers MAX_CONSUMERS
                        max amount of consumers
Training:
  --lr LR               learning rate
  --max_episodes MAX_EPISODES
                        number of episodes
  --game_repetitions GAME_REPETITIONS
                        times to repeat the game
  --save_every_n_episodes SAVE_EVERY_N_EPISODES
                        creates a checkpoint every [n] steps
Inference:
  --examples EXAMPLES   number of examples to test
  --show_plots SHOW_PLOTS
                        whether or not to visualize solution
  --save_plots SAVE_PLOTS
                        whether or not to save visualization
  --save_plots_to SAVE_PLOTS_TO
                        save path for visualizations
  --save_solutions SAVE_SOLUTIONS
                        whether or not to save solution
  --save_solutions_to SAVE_SOLUTIONS_TO
                        save path for solutions
User options:
  --inference_mode INFERENCE_MODE
                        switch to inference mode when model is trained
  --restore_model RESTORE_MODEL
                        whether or not model is retrieved
  --save_to SAVE_TO     saver sub directory
  --restore_from RESTORE_FROM
                        loader sub directory
  --log_dir LOG_DIR     summary writer log directory
```

#### Examples
The network can be used in two different modes: training and inference. The following sections give examples on how
to run the network for training and inference respectively. 

##### Training 

```
run_partitioning_network.py --inference_mode=False --batch_size=128 --game_repetitions=16 --grid_width=8 \ 
--max_length=20 --min_consumers=10 --max_consumers=20 \
--log_dir=summary/partitioning/ --save_to=save/models/8x8_20
```

* `inference_mode=False`: training mode on
* `batch_size=128 `: batch size
* `game_repetitions=16`: indicates how often to sample from the predicted partitioning distribution
* `grid_width=8`: samples consumers from _8x8 grid_
* `min_consumers=10`: samples at least _10 consumers_
* `max_consumers=20`: samples up to _20 consumers_
* `log_dir=summary/partitioning/`: location of tensorboard summaries
* `save_to=save/models/8x8_20`: model checkpoints are stored here 

##### Inference

Once a model is trained you can use it for inference like this:

```
run_partitioning_network.py --inference_mode=True --grid_width=8 --min_consumers=10 --max_consumers=20  
--examples=10 \ 
--restore_from=save/models/8x8_20/actor.ckpt \
--save_plots=True --save_plots_to=plots/ --show_plots=True
--save_solution_to=solutions/8x8_20/solutions.json
 
```
* `inference_mode=True`: inference mode on
* `grid_width=8`: samples consumers from _8x8 grid_
* `min_consumers=10`: samples at least _10 consumers_
* `max_consumers=20`: samples up to _20 consumers_
* `examples=10`: generates and evaluates 10 examples
* `restore_from=save/models/8x8_20/actor.ckpt`: which checkpoint to restore and use for inference

This script automatically generates samples, computes their solutions and visualizes them on a 2D graph. 
Additionally the solutions are saved to a _json_-file for later inspection

* `save_plots=True`: whether to save the plots of the solutions
* `save_plots_to=plots/`: saves the plots to the folder _./plots_ (folder automatically created)
* `show_plots=True`: whether to show the plots of the solutions
* `save_solution_to=solutions/8x8_20/solutions.json`: saves the solutions to the indicated path


## Combinatorial Network

### Model Description

The second neural model to solve the planning problem at hand is a modified version of the network proposed in 
[Neural Combinatorial Optimization with Reinforcement Learning](https://arxiv.org/abs/1611.09940) by Bello et al. In their work the researchers propose using a sequential encoder-decoder network combined with pointer- and glimpse-mechanisms
to approximate solutions to hard combinatorial problems, such as TSP and 0-1-Knapsack. Instead of using a sequential 
directional RNN-encoder, we imply the same non-directional attention mechanism as in the previously described 
partitioning network. This is useful since the order in which the input sequence is presented to the network should not
matter for the production of a good solution. In the following sections, we provide a short description of each network module
implied in this model.

![Diagram2](https://github.com/tkostov/RL_Planning_S/raw/master/img/diagram-2.jpg)

#### Input, Embedding, Encoding

The first part of the network is exactly the same as in the previously described Partitioning Network. However, we include a special 
point in the input sequence that is used as a separator for the partitions that the network has to generate. 
Assume that the network is presented with a problem instance containing 4 points p1, p2, p3, p4. Instead of just inputting p1, ..., p4
we also include a special point s. The network then learns to use the separator s to indicate where
the first partition ends and second partition begins. E.g. the output of the network could be: p1, p4, s, p3, p2. This would be interpreted
as a solutions with two partitions, where the first partition and second partition contain the points 
p1, p4 and p3, p2 respectively. 

#### Decoder

The decoding mechanism is significantly different than the one used in the previously described partitioning network. 
Here at each decoding step, a distribution over the input points - including the aforementioned separator s -is produced. 
From this distribution we draw the next point to visit, until no more point is left. In the decoding step an attention mechanism 
is used in order for the network to focus on the most important parts of the input sequence. The technique is described
in detail in the aforementioned publication. 

#### Reinforcement Learning
As in the previous model, we use reinforcement learning to learn the best partitions and circuits. However, we extended
the current the combinatorial network by a critic module that will help the network learn more efficiently.

##### Actor-Critic Architecture 
A widely used approach to ease learning in reinforcement models is to imply an actor/critic architecture. 
Instead of having just an agent trying to learn a policy, we also include a __critic__ that tries to predict the agent's 
reward solely based on the state of the environment, i.e. the input sequence. The intuition behind 
this idea is that if the critic is able to predict agent's reward without knowing his actions, 
we "punish" the actor. This add an additional driving force to explore the solution space, since,  
in order for the actor to be maximally rewarded, it has to exceed the critics expectations.

##### Reward
In the partitioning network our reward was modeled as the difference between the solution produced by our network 
and the solution produced by a simple heuristic. Here, instead of using a comparison strategy, we use the costs
of the network as reward signal (the cost function is defined in (?)). In order to minimize the magnitude of the 
reward variance, we scale down the cost function by constant factor and subtract the constant expressions. 
We note that these changes to the cost function, do not affect the underlying optimization problem.

#### Optimization
Optimization is performed on actor and critic by using an Adam-Optimizer. In order to avoid gradient explosion 
we clip the gradients length 1.0 before passing them to the optimizer.
 

### Usage

```
run_combinatorial_network.py [-h] [--hidden_dim HIDDEN_DIM] [--num_heads NUM_HEADS]
                             [--num_stacks NUM_STACKS] [--batch_size BATCH_SIZE]
                             [--input_dimension INPUT_DIMENSION] [--grid_width GRID_WIDTH]
                             [--max_length MAX_LENGTH] [--nb_epoch NB_EPOCH]
                             [--lr1_start LR1_START] [--lr1_decay_step LR1_DECAY_STEP]
                             [--lr1_decay_rate LR1_DECAY_RATE] [--alpha ALPHA]
                             [--init_baseline INIT_BASELINE] [--temperature TEMPERATURE]
                             [--C C] [--examples EXAMPLES] [--show_plots SHOW_PLOTS]
                             [--save_plots SAVE_PLOTS] [--save_plots_to SAVE_PLOTS_TO]
                             [--save_solutions SAVE_SOLUTIONS]
                             [--save_solutions_to SAVE_SOLUTIONS_TO]
                             [--input_file INPUT_FILE] [--inference_mode INFERENCE_MODE]
                             [--restore_model RESTORE_MODEL]
                             [--validation_batches VALIDATION_BATCHES]
                             [--restore_from RESTORE_FROM] [--save_to SAVE_TO]
                             [--log_dir LOG_DIR]
                             [--save_every_n_episodes SAVE_EVERY_N_EPISODES]

Configuration file

optional arguments:
  -h, --help            show this help message and exit

Network:
  --hidden_dim HIDDEN_DIM
                        actor LSTM num_neurons
  --num_heads NUM_HEADS
                        actor input embedding
  --num_stacks NUM_STACKS
                        actor LSTM num_neurons

Data:
  --batch_size BATCH_SIZE
                        batch size
  --input_dimension INPUT_DIMENSION
                        consumer dimension
  --grid_width GRID_WIDTH
                        grid width
  --max_length MAX_LENGTH
                        number of consumers

Training:
  --nb_epoch NB_EPOCH   number of epochs to train
  --lr1_start LR1_START
                        actor learning rate
  --lr1_decay_step LR1_DECAY_STEP
                        lr1 decay step
  --lr1_decay_rate LR1_DECAY_RATE
                        lr1 decay rate
  --alpha ALPHA         update factor moving average baseline
  --init_baseline INIT_BASELINE
                        initial baseline - REINFORCE
  --temperature TEMPERATURE
                        pointer net initial temperature
  --C C                 pointer net tan clipping

Inference:
  --examples EXAMPLES   number of examples to test
  --show_plots SHOW_PLOTS
                        whether or not to visualize solution
  --save_plots SAVE_PLOTS
                        whether or not to save visualization
  --save_plots_to SAVE_PLOTS_TO
                        save path for visualizations
  --save_solutions SAVE_SOLUTIONS
                        whether or not to save solution
  --save_solutions_to SAVE_SOLUTIONS_TO
                        save path for solutions
  --input_file INPUT_FILE
                        use input file for problem instance (csv file with 3
                        columns)

User options:
  --inference_mode INFERENCE_MODE
                        switch to inference mode when model is trained
  --restore_model RESTORE_MODEL
                        whether or not model is retrieved
  --validation_batches VALIDATION_BATCHES
                        number of batches to use for validation
  --restore_from RESTORE_FROM
                        loader sub directory
  --save_to SAVE_TO     saver sub directory
  --log_dir LOG_DIR     summary writer log directory
  --save_every_n_episodes SAVE_EVERY_N_EPISODES
                        saves a checkpoint every [n] steps
```

#### Training

Many options should look familiar, as they are also used in the partitioning network. Here is how you start the training 
of network:

```
run_combinatorial_network.py --inference_mode=False --batch_size=256 --grid_width=12 --validation_batches=10 \ 
--max_length=50 --log_dir=summary/combinatorial/ --save_to=save/models/12x12_50 \
--num_hidden=256
```

* `inference_mode=False`: training mode on
* `batch_size=256 `: batch size
* `grid_width=12`: samples consumers from _12x12 grid_
* `validation_batches=10`: uses _10 instances for validation_, by plotting the solution generated by the network next to the solution of the heuristic.
   Plots are stored in {save_to}/plots/. In the same folder, results.csv is generated and contains the costs of the validation instances over training time.  
* `max_length=50`: samples _50 consumers_
* `num_hidden=256`: number of dimension of hidden LSTM state 
* `log_dir=summary/combinatorial/`: location of tensorboard summaries
* `save_to=save/models/12x12_50`: model checkpoints are stored here 

Suggestion: The more consumers, the larger the batch size and the hidden dimensions should be. 

#### Inference

After training, the models that have been saved to the directory indicated in `save_to` are restored and used
for inference like this:
 
```
run_combinatorial_network.py --inference_mode=True --batch_size=512 --grid_width=12 --max_length=50 \  
--examples=10 \ 
--restore_model=True
--restore_from=save/models/12x12_50/actor.ckpt \
--save_plots=True --save_plots_to=plots/ --show_plots=True
--save_solution_to=solutions/8x8_20/solutions.csv
 
```
* `inference_mode=True`: inference mode on
* `batch_size=512`: Samples 512 solutions from each instance. The larger the batch, the better the networks performance 
at the cost of computation time
* `grid_width=12`: samples consumers from _12x12 grid_
* `max_length=50`: samples _50 consumers_
* `examples=10`: generates and evaluates 10 examples
* `restore_from=save/models/8x8_20/actor.ckpt`: which checkpoint to restore and use for inference

This script automatically generates samples, computes their solutions and visualizes them on a 2D graph. 
Additionally the solutions are saved to _csv_-files for later inspection

* `save_plots=True`: whether to save the plots of the solutions
* `save_plots_to=plots/`: saves the plots to the folder _./plots_ (folder automatically created)
* `show_plots=True`: whether to show the plots of the solutions
* `save_solution_to=solutions/12x12_50/{i}.csv`: saves the solutions to the indicated path (i=0,1,2,3, ..., examples-1)

# Conclusion

We performed experiments with both models and came to the following conclusions.

## Partitioning Network
As of now we weren't able to produce any useful results by using the __Partitioning Network__. However, this might be
due to the high number of hyperparameters that have to be tuned. Further research is needed in order to find a suitable 
configuration for the network and workout the issues of this approach. We are confident, that if more work is put into optimization
we can produce useful results using this architecture. 

## Combinatorial Network
On the other hand, using the combinatorial network shows great potential for the generation of 
very competitive solutions. We tested trained and tested the network with a 20x20 grid with 38 consumers. 
Even after a few thousand training iterations, the network starts to produce sensible solutions. Experiments with 15
consumers indicate that our network on expectation produces less costly solution than the heuristic.  
However, experiments with higher numbers of consumers show that the network's learning progress is not stable, as its
performance still oscillates between excellent and poor solutions even after thousands of epochs. 

This problem can be tackled by 

1) choosing a larger batch size during training, which, as of now, we could not do due to memory limitations in our hardware. (Need to change the code a bit to make it run on multiple GPUs)
2) applying a more stable reward baseline, such as the costs returned by the heuristic
3) enlarge the attention stack, which might be needed to tackle the much higher combinatorial 
   complexity of our problem, compared to just TSP

The model shows a potential, which can be fully used with better hardware.   
