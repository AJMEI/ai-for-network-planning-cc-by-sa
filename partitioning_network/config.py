import argparse

parser = argparse.ArgumentParser(description='Configuration file')
arg_lists = []


def add_argument_group(name):
    arg = parser.add_argument_group(name)
    arg_lists.append(arg)
    return arg


def str2bool(v):
    return v.lower() in ('true', '1')


# Network
net_arg = add_argument_group('Network')
net_arg.add_argument('--lstm_state_size', type=int, default=128, help='LSTM state size')
net_arg.add_argument('--use_attention', type=str2bool, default=True, help='whether or not to use attention mechanism')

# Data
data_arg = add_argument_group('Data')
data_arg.add_argument('--batch_size', type=int, default=128, help='batch size')
data_arg.add_argument('--grid_width', type=int, default=7, help='grid with')
data_arg.add_argument('--min_consumers', type=int, default=10, help='min amount of consumers]')
data_arg.add_argument('--max_consumers', type=int, default=10, help='max amount of consumers')

# Training / test parameters
train_arg = add_argument_group('Training')
train_arg.add_argument('--lr', type=float, default=1e-4, help='learning rate')
train_arg.add_argument('--max_episodes', type=int, default=100000, help='number of episodes')
train_arg.add_argument('--game_repetitions', type=int, default=1, help='times to repeat the game')
train_arg.add_argument('--save_every_n_episodes', type=int, default=2, help='creates a checkpoint every [n] steps')

inference_arg = add_argument_group('Inference')
inference_arg.add_argument('--examples', type=int, default=20, help='number of examples to test')
inference_arg.add_argument('--show_plots', type=str2bool, default=True, help='whether or not to visualize solution')
inference_arg.add_argument('--save_plots', type=str2bool, default=True, help='whether or not to save visualization')
inference_arg.add_argument('--save_plots_to', type=str, default='plots/newest/', help='save path for visualizations')

inference_arg.add_argument('--save_solutions', type=str2bool, default=True, help='whether or not to save solution')
inference_arg.add_argument('--save_solutions_to', type=str, default='solutions/solutions_newest.json',
                           help='save path for solutions')

# Misc
misc_arg = add_argument_group('User options')
misc_arg.add_argument('--inference_mode', type=str2bool, default=False,
                      help='switch to inference mode when model is trained')
misc_arg.add_argument('--restore_model', type=str2bool, default=False, help='whether or not model is retrieved')
misc_arg.add_argument('--save_to', type=str, default='save/models/partitioning_rewards_min', help='saver sub directory')
misc_arg.add_argument('--restore_from', type=str, default='save/models/partitioning_rewards_min/model.ckpt-1520', help='loader sub directory')
misc_arg.add_argument('--log_dir', type=str, default='summary/partitioning', help='summary writer log directory')
misc_arg.add_argument('--input_file', type=str, default='',
                      help='use input file for problem instance (csv file with 3 columns)')


def get_config():
    config, unparsed = parser.parse_known_args()

    if hasattr(config, 'help') and config.help:
        config.print_help()
        exit(0)

    return config, unparsed
