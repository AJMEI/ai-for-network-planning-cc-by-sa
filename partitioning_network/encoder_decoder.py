import tensorflow as tf
from tensorflow.python.ops.rnn_cell_impl import LSTMStateTuple

from combinatorial_network.critic import Critic
from combinatorial_network.encoder import AttentiveEncoder
from combinatorial_network.game import GameCombinatorialNetwork


def variable_summaries(name, var, with_max_min=False):
    with tf.name_scope(name):
        mean = tf.reduce_mean(var)
        tf.summary.scalar('mean', mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('stddev', stddev)

        if with_max_min:
            tf.summary.scalar('max', tf.reduce_max(var))
            tf.summary.scalar('min', tf.reduce_min(var))


class EncoderDecoder:
    def __init__(self, state_size_lstm=64, use_attention=True, inference_mode=False, init_baseline=20.0):
        self.state_size_lstm = state_size_lstm
        self.batch_size_ = tf.placeholder(tf.int32, [], name="batch_size")
        self.sequence_length_ = tf.placeholder(tf.int32, [], name="sequence_length")

        # Inputs from game
        self.sampled_partitions = None
        self.costs = None

        # sampled grid
        self.batch_input_ = tf.placeholder(tf.float32, [None, None, 3], name="input_layer")
        self.batch_input_transformed = None

        # Train step
        self.train_opt_actor = None
        self.neg_log_prob = None

        # Partition distribution layer#
        self.distribution = None

        self.alpha = tf.constant(0.99, dtype=tf.float32)
        self.avg_baseline = tf.Variable(init_baseline, trainable=False,
                                        name="moving_avg_baseline")

        self.moving_average_operation = None
        self.rewards_with_baseline = None
        self.heuristic_costs = None
        self.our_costs = None
        self.rewards = None

        # actor
        self.loss_actor = None
        self.loss_critic = None

        # critic params
        self.critic = None

        self.train_step_actor = None
        self.train_step_critic = None
        self.train_opt_critic = None

        self.lr_actor = None
        self.lr_actor_start = 0.001
        self.lr_actor_decay_step = 5000
        self.lr_actor_decay_rate = 0.96

        self.lr_critic = None
        self.lr_critic_start = 0.001
        self.lr_critic_decay_step = 5000
        self.lr_critic_decay_rate = 0.96

        self.global_step = tf.Variable(0, trainable=False, name='global_step')
        self.global_step2 = tf.Variable(0, trainable=False, name='global_step2')

        self.reward_baseline = None
        self.merged = None

        self.inference_mode = inference_mode
        self.use_attention = use_attention
        self.build_network()

    def build_network(self):
        # Actor
        with tf.variable_scope('encoder'):
            # decoder_first_input, output_dim = self.build_encoder_1()
            if self.use_attention:
                decoder_first_input, output_dim = self.build_encoder_2()
            else:
                decoder_first_input, output_dim = self.build_encoder_1()

        with tf.variable_scope('decoder'):
            decoder_outputs = self.build_decoder(decoder_first_input, output_dim)

        with tf.variable_scope('softmax'):
            logits = self.build_prediction(decoder_outputs, output_dim)

        # Critic
        with tf.variable_scope('critic'):
            self.build_critic()

        self.build_reward(logits)

        # Moving average for reward baseline
        with tf.variable_scope('reward_baseline'):
            self.build_reward_baseline()

        # loss for actor
        with tf.name_scope("loss_actor"):
            # The lower the cross-entropy, the better the predictions fit the labels
            self.build_loss_actor(logits)

        with tf.name_scope("optimization"):
            self.lr_actor = tf.train.exponential_decay(self.lr_actor_start, self.global_step,
                                                       self.lr_actor_decay_step,
                                                       self.lr_actor_decay_rate, staircase=False,
                                                       name="learning_rate_actor")
            # Optimizer
            self.train_opt_actor = tf.train.AdamOptimizer(learning_rate=self.lr_actor, beta1=0.9, beta2=0.99,
                                                          epsilon=0.0000001)

            gvs = self.train_opt_actor.compute_gradients(self.loss_actor)
            capped_gradients_actor = [(tf.clip_by_norm(grad, 1.), var) for grad, var in gvs if grad is not None]
            self.train_step_actor = self.train_opt_actor.apply_gradients(capped_gradients_actor,
                                                                         global_step=self.global_step)

        with tf.name_scope('loss_critic'):
            self.build_optimization_critic()

        self.build_visualizations()

        self.merged = tf.summary.merge_all()

    def build_visualizations(self):
        with tf.name_scope("cross_entropy"):
            variable_summaries("neg_log_prob", self.neg_log_prob, with_max_min=True)

        with tf.name_scope('environment'):
            # self.heuristic_rewards_ = tf.log(self.heuristic_rewards_)
            # variable_summaries('reward', self.rewards_stats[:, 0], True)
            variable_summaries('reward', self.rewards, True)

        with tf.name_scope('network_costs'):
            variable_summaries("our_costs", self.our_costs, True)

        with tf.name_scope('heuristic_costs'):
            variable_summaries("heuristic_costs", self.heuristic_costs, True)

        with tf.name_scope('cost_comparison'):
            has_better_solution = tf.cast(
                tf.sign(tf.reduce_min(self.heuristic_costs) - tf.reduce_min(self.our_costs)), dtype=tf.float32)

            variable_summaries('has_better_solution', has_better_solution)

    def build_critic(self):
        hidden_dim = 128
        critic_encoder = AttentiveEncoder(batch_size=self.batch_size_, max_length=None, input_dimension=3,
                                          hidden_dim=hidden_dim,
                                          num_heads=16, num_stacks=3, is_training=not self.inference_mode)
        self.critic = Critic(critic_encoder)
        self.critic.predict_rewards(self.batch_input_)

    def build_reward_baseline(self):
        reward_mean, reward_var = tf.nn.moments(self.rewards, axes=[0])
        self.moving_average_operation = tf.assign(self.avg_baseline,
                                                  self.alpha * self.avg_baseline + (1.0 - self.alpha) * reward_mean)

    def build_optimization_critic(self):
        # Critic learning rate
        self.lr_critic = tf.train.exponential_decay(self.lr_critic_start, self.global_step2, self.lr_critic_decay_step,
                                                    self.lr_critic_decay_rate, staircase=False,
                                                    name="learning_rate_critic")
        # Optimizer
        self.train_opt_critic = tf.train.AdamOptimizer(learning_rate=self.lr_critic, beta1=0.9, beta2=0.99,
                                                       epsilon=0.0000001)

        # Loss
        weights_ = 1.0  # weights_ = tf.exp(self.log_softmax-tf.reduce_max(self.log_softmax)) # probs / max_prob
        self.loss_critic = tf.losses.mean_squared_error(self.rewards - self.avg_baseline,
                                                        self.critic.predictions,
                                                        weights=weights_)
        # Minimize step
        gvs2 = self.train_opt_critic.compute_gradients(self.loss_critic)
        capped_gradients_critic = [(tf.clip_by_norm(grad, 1.), var) for grad, var in gvs2 if grad is not None]
        self.train_step_critic = self.train_opt_actor.apply_gradients(capped_gradients_critic,
                                                                      global_step=self.global_step2)
        tf.summary.scalar('loss_critic', self.loss_critic)
        variable_summaries('critic_predictions', self.critic.predictions)

    def build_loss_actor(self, logits):
        beta = 0.01
        entropy = tf.reduce_mean(- tf.reduce_sum(self.distribution * tf.log(self.distribution), axis=2), axis=1)
        self.neg_log_prob = self.build_negative_log_probability(logits)
        self.rewards_with_baseline = tf.stop_gradient(self.rewards - self.avg_baseline -
                                                      self.critic.predictions)
        loss = -self.neg_log_prob * self.rewards_with_baseline - beta*entropy  #  Add baseline
        self.loss_actor = tf.reduce_mean(loss)

        tf.summary.scalar("loss", self.loss_actor)

    def build_negative_log_probability(self, logits):
        neg_log_prob = tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=self.sampled_partitions)
        neg_log_prob = tf.reduce_sum(neg_log_prob, axis=1)
        # The lower the cross-entropy, the better the predictions fit the the labels
        # logits_ = tf.reshape(logits, [-1, 2])
        # chosen_part = tf.reshape(self.chosen_partition_, [-1, 2])
        # neg_log_prob = tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits_, labels=chosen_part)
        return neg_log_prob

    def build_prediction(self, decoder_outputs, output_dim):
        outputs = tf.reshape(decoder_outputs, [-1, output_dim * 2])
        outputs = tf.layers.dense(outputs, 256, tf.nn.leaky_relu, name="readout")
        outputs = tf.layers.dense(outputs, 256, tf.nn.leaky_relu, name="readout2")
        logits = tf.layers.dense(outputs, 2, name="to_2dim")
        logits = tf.reshape(logits, [self.batch_size_, -1, 2])
        distribution = tf.nn.softmax(logits)
        self.distribution = tf.reshape(distribution, [self.batch_size_, -1, 2])  # BS x SeqLen x 2
        tf.summary.histogram("distribution", distribution)
        return logits

    def build_decoder(self, decoder_first_input, output_dim):
        decoder_cell_fw = tf.nn.rnn_cell.LSTMCell(output_dim)
        decoder_cell_bw = tf.nn.rnn_cell.LSTMCell(output_dim)
        (output_fw, output_bw), decoder_last_state = \
            tf.nn.bidirectional_dynamic_rnn(decoder_cell_fw,
                                            decoder_cell_bw,
                                            dtype=tf.float32,
                                            inputs=self.batch_input_,
                                            initial_state_fw=decoder_first_input,
                                            initial_state_bw=decoder_first_input)
        decoder_outputs = tf.concat([output_fw, output_bw], axis=2)
        return decoder_outputs

    def build_encoder_2(self):
        hidden_dim = 128
        encoder = AttentiveEncoder(batch_size=self.batch_size_,
                                   max_length=None,
                                   input_dimension=3,
                                   hidden_dim=hidden_dim,
                                   num_heads=16, num_stacks=3, is_training=not self.inference_mode)

        initializer = tf.contrib.layers.xavier_initializer()

        encoder_output = encoder.encode(self.batch_input_)

        decoder_initial_state = tf.get_variable('decoder_first_state', shape=[1, hidden_dim],
                                                initializer=initializer)

        c, h = tf.tile(decoder_initial_state, [self.batch_size_, 1]), tf.reduce_mean(encoder_output, axis=1)
        return LSTMStateTuple(c, h), hidden_dim

    def build_encoder_1(self):
        # do some processing!
        weight_coef = tf.tile(tf.reshape(self.batch_input_[:, :, 2], [self.batch_size_, -1, 1]), [1, 1, 2])
        # Weight coef
        weighted_positions = self.batch_input_[:, :, :2] * weight_coef
        self.batch_input_transformed = tf.concat([self.batch_input_, weighted_positions], axis=2)
        self.batch_input_transformed = tf.reshape(self.batch_input_transformed, [-1, 5])
        self.batch_input_transformed = tf.layers.dense(self.batch_input_transformed, 128, tf.nn.leaky_relu,
                                                       name="inp_embedding")
        self.batch_input_transformed = tf.reshape(self.batch_input_transformed,
                                                  [self.batch_size_, self.sequence_length_, 128])
        # build forward and backward for RNN layer
        encoder_cell_fw = tf.nn.rnn_cell.LSTMCell(self.state_size_lstm)
        encoder_cell_fw_initial_state = encoder_cell_fw.zero_state(self.batch_size_, tf.float32)
        encoder_cell_bw = tf.nn.rnn_cell.LSTMCell(self.state_size_lstm)
        encoder_cell_bw_initial_state = encoder_cell_bw.zero_state(self.batch_size_, tf.float32)
        encoder_outputs, encoder_final_state = tf.nn.bidirectional_dynamic_rnn(
            encoder_cell_fw,
            encoder_cell_bw,
            dtype=tf.float32,
            inputs=self.batch_input_,
            initial_state_fw=encoder_cell_fw_initial_state,
            initial_state_bw=encoder_cell_bw_initial_state)
        c = tf.concat([encoder_final_state[0].c, encoder_final_state[1].c], axis=1)
        h = tf.concat([encoder_final_state[0].h, encoder_final_state[1].h], axis=1)
        # TODO transform thru state
        ######################
        # c = tf.layers.dense(c, self.state_size_lstm*2, tf.nn.leaky_relu, name="c1")
        # c = tf.layers.dense(c, self.state_size_lstm*2, tf.nn.leaky_relu, name='c2')
        h = tf.layers.dense(h, self.state_size_lstm * 2, tf.nn.leaky_relu, name="h1")
        h = tf.layers.dense(h, self.state_size_lstm * 2, tf.nn.leaky_relu, name='h2')
        #####################
        encoder_final_state_both = LSTMStateTuple(c, h)
        return encoder_final_state_both, self.state_size_lstm * 2

    def build_reward(self, logits):
        with tf.name_scope('reward_function'):
            multinomial = tf.distributions.Multinomial(total_count=1.0, logits=logits)

            self.sampled_partitions = multinomial.sample()

            self.costs = tf.py_func(GameCombinatorialNetwork.play_batch_game,
                                    [self.sampled_partitions, self.batch_input_],
                                    tf.float32,
                                    name='rewards_func')
            self.heuristic_costs = self.costs[:, 1]
            self.our_costs = self.costs[:, 0]
            self.rewards = self.costs[:, 0]
            # self.rewards = tf.expand_dims(self.costs[:, 0], axis=1)
            # self.rewards = tf.tile(self.rewards, [1, self.sequence_length_])
            self.rewards = tf.log(self.rewards)
