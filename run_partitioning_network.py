# !/usr/bin/env python
# -*- coding: utf-8 -*-
import csv
import json
import os
from datetime import datetime

import numpy as np
import tensorflow as tf
from matplotlib.pyplot import savefig
from tqdm import tqdm

from combinatorial_network.game import GameCombinatorialNetwork
from partitioning_network.config import get_config
from partitioning_network.encoder_decoder import EncoderDecoder
from partitioning_network.game import Game, Sampler
from utils.misc import compute_optimal_trip, visualize_2d_trip

if __name__ == '__main__':
    config, _ = get_config()

    max_episodes = config.max_episodes
    encoder_decoder = EncoderDecoder(config.lstm_state_size, config.inference_mode)

    # Add ops to save and restore all the variables.
    save_every_n_episodes = config.save_every_n_episodes

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        saver = tf.train.Saver(var_list=tf.global_variables(), max_to_keep=4)

        now = datetime.now()
        time = now.strftime("%Y%m%d-%H%M%S")
        train_writer = tf.summary.FileWriter(config.log_dir + '/train/%s' % time, sess.graph)

        # Game hyperparameters
        grid_width = config.grid_width

        sampler = Sampler()
        game = Game(size=config.grid_width)

        # Training hyperparameters
        times = config.game_repetitions
        batch_size = config.batch_size
        path = os.path.dirname(os.path.realpath(__file__))

        #        if config.restore_model:
        #            saver.restore(sess, config.restore_from)

        if not config.inference_mode:
            os.makedirs(config.save_to, exist_ok=True)
            for epoch in tqdm(range(max_episodes)):
                # Get instance
                if config.min_consumers != config.max_consumers:
                    sequence_length = np.random.random_integers(config.min_consumers, config.max_consumers)
                else:
                    sequence_length = config.min_consumers

                batch_input, decorrelated_batch, cost_batch = game.get_repeated_instance(batch_size, sequence_length)

                # Train with the instance for 25 epochs
                ep_p_epoch = 1  # episodes per epo
                for episode in range(ep_p_epoch):
                    # Get problem instance
                    # print("Epoch %i: Episode: %i | Sequence length: %i " % (epoch, episode, sequence_length))

                    # Forward pass. Calculate the action distributions
                    # partition_distribution = sess.run(encoder_decoder.distribution,
                    #                                   feed_dict={encoder_decoder.batch_size_: batch_size,
                    #                                              encoder_decoder.sequence_length_: sequence_length,
                    #                                              encoder_decoder.batch_input_: decorrelated_batch})

                    # # Evaluate the distributions
                    # game_tuple = game.play_batch_game(partition_distribution,
                    #                                   batch_input, times=1,
                    #                                   normalized_batch_inp_=decorrelated_batch, cost_batch=cost_batch)
                    #
                    # chosen_partitions, rewards, instance, instance_normalized, our_costs = game_tuple
                    #
                    # print("Rewards Mean: {}".format(np.mean(rewards)))
                    #
                    feed = feed_dict = \
                        {
                            encoder_decoder.batch_input_: decorrelated_batch,
                            encoder_decoder.batch_size_: batch_size,
                            encoder_decoder.sequence_length_: sequence_length
                        }

                    summary, _, _, _ = sess.run([encoder_decoder.merged,
                                                 encoder_decoder.moving_average_operation,
                                                 encoder_decoder.train_step_actor,
                                                 encoder_decoder.train_step_critic
                                                 ], feed)

                    train_writer.add_summary(summary, epoch * ep_p_epoch + episode)

                    if epoch > 0 and (epoch % save_every_n_episodes) == 0:
                        saver.save(sess, "%s/model.ckpt" % config.save_to, global_step=epoch)
            train_writer.close()
        else:

            examples = config.examples
            batch_size = config.batch_size
            grid_width = config.grid_width
            game = Game()
            solutions = []

            if config.save_plots:
                os.makedirs(config.save_plots_to, exist_ok=True)

            use_input_file = len(config.input_file) > 0
            if not use_input_file:
                iterations = range(examples)
            else:
                iterations = [0xdeadbeef]
            better = 0
            for i in iterations:
                if not use_input_file:
                    sequence_length = np.random.random_integers(config.min_consumers, config.max_consumers)
                    batch_input, decorrelated_batch, cost_batch = game.get_instance(1, sequence_length)
                else:
                    with open(config.input_file, 'r') as file:
                        reader = csv.reader(file, delimiter=',')
                        batch_input = []
                        for row in reader:
                            new_row = [x for x in map(float, row)]

                            batch_input.append(new_row)

                        batch_input = np.array(batch_input)
                        decorrelated_batch = np.expand_dims(Sampler.decorrelate_instance(batch_input), axis=0)
                        batch_input = np.expand_dims(batch_input, axis=0)
                        cost_batch = game.calculate_cost_batch(batch_input)
                        sequence_length = batch_input.shape[2]

                batch_input = np.tile(batch_input, [batch_size, 1, 1])
                decorrelated_batch = np.tile(decorrelated_batch, [batch_size, 1, 1])
                cost_batch = np.tile(cost_batch, [batch_size, 1, 1])

                partition_distribution = sess.run(encoder_decoder.distribution,
                                                  feed_dict={encoder_decoder.batch_size_: batch_size,
                                                             encoder_decoder.sequence_length_: sequence_length,
                                                             encoder_decoder.batch_input_: decorrelated_batch})

                game_tuple = game.play_batch_game(partition_distribution,
                                                  batch_input, times=1,
                                                  normalized_batch_inp_=decorrelated_batch, cost_batch=cost_batch)

                chosen_partitions, rewards, instance, instance_normalized, _ = game_tuple

                j = np.argmin(rewards[:, 0])

                best_partition = chosen_partitions[j, :, 0]
                p1 = batch_input[0, np.where(best_partition), :][0]
                p2 = batch_input[0, np.where(1 - best_partition), :][0]

                _, route1, _ = compute_optimal_trip(p1)
                _, route2, _ = compute_optimal_trip(p2)

                opt_trip1 = p1[route1, :]
                opt_trip2 = p2[route2, :]

                if config.show_plots:
                    visualize_2d_trip([opt_trip1, opt_trip2])

                if config.save_plots and config.show_plots:
                    savefig("plots/%s-%i.png" % (time, i))

                points = np.vstack([p1, p2])
                costs = float(np.log(GameCombinatorialNetwork.compute_partition_costs(p1, p2, cost_function=None,
                                                                                      input_dimension=3)))

                costs_heuristic = float(cost_batch[j])

                solution = {'points': points.tolist(),
                            'partition_1': p1.tolist(),
                            'partition_2': p2.tolist(),
                            'route_1': route1,
                            'route_2': route2,
                            'costs': costs,
                            'costs_heuristic': costs_heuristic}

                solutions.append(solution)
                print('costs: %.2f' % costs)
                print('heuristic costs: %.2f' % costs_heuristic)
                print('diff: %.2f' % (costs_heuristic - costs))

                better += int(costs < costs_heuristic)
            print('%% of better solutions: %.2f' % ((better / examples) * 100))
            if config.save_solutions:
                dir_save_file = os.path.dirname(config.save_solutions_to)
                os.makedirs(dir_save_file, exist_ok=True)

                with open(config.save_solutions_to, 'w') as file:
                    json.dump(solutions, file)
